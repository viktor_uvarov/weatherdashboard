#Depending on the operating system of the host machines(s) that will build or run the containers, the image specified in the FROM statement may need to be changed.
#For more information, please see https://aka.ms/containercompat

#FROM mcr.microsoft.com/dotnet/core/aspnet:3.0-nanoserver-1903 AS base
FROM mcr.microsoft.com/dotnet/core/aspnet:3.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

#FROM mcr.microsoft.com/dotnet/core/sdk:3.0-nanoserver-1903 AS build
FROM mcr.microsoft.com/dotnet/core/sdk:3.0 AS build
WORKDIR "/WeatherDashboard.Models"
COPY "./WeatherDashboard.Models/." .
WORKDIR "/WeatherDashboard.DAL"
COPY "./WeatherDashboard.DAL/." .
WORKDIR "/WeatherDashboard.BusinessLayer"
COPY "./WeatherDashboard.BusinessLayer/." .
WORKDIR "/src"
COPY ["./WeatherDashboard/WeatherDashboard.csproj", ""]
RUN dotnet restore "./WeatherDashboard.csproj"
COPY "./WeatherDashboard/." .
WORKDIR "/src/."
RUN dotnet build "WeatherDashboard.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "WeatherDashboard.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "WeatherDashboard.dll"]