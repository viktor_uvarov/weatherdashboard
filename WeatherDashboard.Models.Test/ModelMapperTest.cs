using AutoFixture;
using AutoMapper;
using NUnit.Framework;
using WeatherDashboard.Models.DbModels;
using WeatherDashboard.Models.Models.Results;
using ModelAutoMapper = WeatherDashboard.Models.AutoMapper;

namespace WeatherDashboard.Models.Test
{
    public class ModelMapperTest
    {
        private IMapper _mapper;
        private Fixture _fixture;

        public ModelMapperTest()
        {
            _mapper = ModelAutoMapper.ModelMapper.GetMapper();
            _fixture = new Fixture();
        }

        [Test]
        public void ModelMapper_Check_WeatherData_WeatherDataDTO()
        {
            var source = _fixture.Create<WeatherData>();
            var dest = _mapper.Map<WeatherDataDTO>(source);

            Assert.AreEqual(source.Name, dest.StationName);
            Assert.AreEqual(source.Station, dest.StationCode);
        }
    }
}