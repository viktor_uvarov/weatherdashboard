﻿namespace WeatherDashboard.BusinessLayer.Test.Services
{
    using AutoMapper;
    using Moq;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using WeatherDashboard.BusinessLayer.Repositories;
    using WeatherDashboard.BusinessLayer.Services;
    using WeatherDashboard.Models.AutoMapper;
    using WeatherDashboard.Models.Models.Queries;
    using WeatherDashboard.Models.Models.Results;

    public class WeatherStationServiceTest
    {
        private Mock<IRepositoryContext> _repoContext;
        private Mock<IWeatherDataRepository> _mockWeatherDataRepository;
        private IMapper _mapper;
        private WeatherStationService _service;

        public WeatherStationServiceTest()
        {
            _mapper = ModelMapper.GetMapper();
            _mockWeatherDataRepository = new Mock<IWeatherDataRepository>();
            _repoContext = new Mock<IRepositoryContext>();
            _repoContext.Setup(v => v.WeatherDataRepository).Returns(_mockWeatherDataRepository.Object);
            _service = new WeatherStationService(_repoContext.Object, _mapper);
        }

        [SetUp]
        public void Setup()
        {
            _mockWeatherDataRepository.Reset();
        }

        [Test]
        public async Task WeatherStationServiceTest_FindProvincesAsync_RepositoryCalled()
        {
            var data = new List<WeatherProviceDTO>();
            _mockWeatherDataRepository.Setup(v => v.FindProvincesAsync()).ReturnsAsync(data);
            var ret = await _service.FindProvincesAsync();
            _mockWeatherDataRepository.Verify(v => v.FindProvincesAsync(), Times.Once);
            Assert.AreEqual(data, ret);
        }

        [Test]
        public async Task WeatherStationServiceTest_FindStationsAsync_RepositoryCalledd()
        {
            var data = new List<WeatherStationListDTO>();
            _mockWeatherDataRepository.Setup(v => v.FindStationsAsync(It.IsAny<FindStationsQuery>())).ReturnsAsync(data);
            var query = new FindStationsQuery();
            var ret = await _service.FindStationsAsync(query);
            _mockWeatherDataRepository.Verify(v => v.FindStationsAsync(It.Is<FindStationsQuery>(vv => vv == query)), Times.Once);
            Assert.AreEqual(data, ret);
        }

        [Test]
        public async Task WeatherStationServiceTest_GetStationDataAsync_RepositoryCalled()
        {
            var data = new WeatherStationDataDTO();
            var details = new List<WeatherStationDetailsDTO>();
            _mockWeatherDataRepository.Setup(v => v.GetStationDataAsync(It.IsAny<GetStationDataQuery>())).ReturnsAsync(data);
            _mockWeatherDataRepository.Setup(v => v.GetStationDetailsAsync(It.IsAny<GetStationDataQuery>())).ReturnsAsync(details);
            var query = new GetStationDataQuery();
            var ret = await _service.GetStationDataAsync(query);
            _mockWeatherDataRepository.Verify(v => v.GetStationDataAsync(It.Is<GetStationDataQuery>(vv => vv == query)), Times.Once);
            _mockWeatherDataRepository.Verify(v => v.GetStationDetailsAsync(It.Is<GetStationDataQuery>(vv => vv == query)), Times.Once);
            Assert.AreEqual(data, ret);
            Assert.AreEqual(data.Details, details);
        }
    }
}
