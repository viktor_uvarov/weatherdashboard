﻿namespace WeatherDashboard.BusinessLayer.Test.Repositories
{
    using AutoFixture;
    using NUnit.Framework;
    using System.Linq;
    using System.Threading.Tasks;
    using WeatherDashboard.BusinessLayer.Repositories;
    using WeatherDashboard.Models.DbModels;
    using WeatherDashboard.Models.Models.Queries;
    using WeatherDashboard.Models.Models.Results;

    public class WeatherDataRepositoryTest : BaseRepositoryTest<WeatherData, WeatherDataRepository>
    {
        public WeatherDataRepositoryTest() : base()
        {
        }
        [SetUp]
        public void Setup()
        {
            _data = _fixture.CreateMany<WeatherData>(10).ToList();
            _data[1].Province = _data[0].Province;
            _repo = new WeatherDataRepository(() => _generator.InitTestContext().FillData(_data).Context, _mapper);
        }

        [Test]
        public async Task WeatherDataRepositoryTest_FindProvincesAsync_GetAllElements()
        {
            var ret = await _repo.FindProvincesAsync();
            Assert.AreEqual(_data.Select(v => v.Province).Distinct().Count(), ret.Count());
            Assert.AreEqual(_data.Select(v => v.Province).Distinct().OrderBy(v => v).First(), ret.First().ProviceCode);
        }

        #region FindStationsAsync
        [Test]
        public async Task WeatherDataRepositoryTest_FindStationsAsync_GetElements_ByProvice()
        {
            var query = new FindStationsQuery { Province = _data[0].Province };
            var ret = await _repo.FindStationsAsync(query);
            Assert.AreEqual(2, ret.Count());
        }

        [Test]
        public async Task WeatherDataRepositoryTest_FindStationsAsync_GetNoElements_ByCustomProvice()
        {
            var query = new FindStationsQuery { Province = _fixture.Create<string>() };
            var ret = await _repo.FindStationsAsync(query);
            Assert.AreEqual(0, ret.Count());
        }

        [Test]
        public async Task WeatherDataRepositoryTest_FindStationsAsync_CheckMapping()
        {
            var query = new FindStationsQuery { Province = _data.First().Province };
            var ret = await _repo.FindStationsAsync(query);
            var d = _data.Where(v => v.Province == _data[0].Province).OrderBy(v => v.Name).First();
            var r = ret.First();
            Assert.AreEqual(d.Name, r.StationName);
            Assert.AreEqual(d.Station, r.StationCode);
            Assert.AreEqual(d.Mdate.Year, r.YearStart);
            Assert.AreEqual(d.Mdate.Year, r.YearFinish);
        }
        #endregion

        #region GetStationDataAsync
        [Test]
        public async Task WeatherDataRepositoryTest_GetStationDataAsync_GetElement_ByCode()
        {
            var query = new GetStationDataQuery { Code = _data[0].Station };
            var ret = await _repo.GetStationDataAsync(query);
            Assert.IsNotNull(ret);
            Assert.AreEqual(_data[0].Name, ret.StationName);
        }

        [Test]
        public async Task WeatherDataRepositoryTest_GetStationDataAsync_GetNullElement()
        {
            var query = new GetStationDataQuery { Code = _fixture.Create<string>() };
            var ret = await _repo.GetStationDataAsync(query);
            Assert.IsNull(ret);
        }

        [Test]
        public async Task WeatherDataRepositoryTest_GetStationDataAsync_CheckMapping()
        {
            var query = new GetStationDataQuery { Code = _data[0].Station };
            var ret = await _repo.GetStationDataAsync(query);
            var d = _data[0];
            Assert.AreEqual(d.Name, ret.StationName);
            Assert.AreEqual(d.Longtitude, ret.Longtinude);
            Assert.AreEqual(d.Latitude, ret.Latitude);
            Assert.AreEqual(d.Latitude, ret.Latitude);
            Assert.AreEqual(d.Station, ret.StationCode);
            Assert.AreEqual(d.Province, ret.StationProvinceCode);
            Assert.AreEqual(d.Mdate, ret.Start);
            Assert.AreEqual(d.Mdate, ret.Finish);

        }
        #endregion

        #region GetStationDataAsync
        // ToDo: Not covered
        #endregion
    }
}