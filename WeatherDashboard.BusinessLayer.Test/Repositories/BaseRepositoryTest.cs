﻿namespace WeatherDashboard.BusinessLayer.Test.Repositories
{
    using AutoMapper;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using WeatherDashboard.BusinessLayer.Repositories;
    using WeatherDashboard.DAL;
    using WeatherDashboard.DAL.Test;
    using WeatherDashboard.Models.AutoMapper;
    using WeatherDashboard.Models.DbModels;
    using WeatherDashboard.Models.Models.Results;

    public class BaseRepositoryTest
    {
        private DbContextGenerator _generator;
        private IMapper _mapper;
        private WeatherDataRepository _repo;
        private IList<WeatherData> _data;
        
        public BaseRepositoryTest()
        {
            _mapper = ModelMapper.GetMapper();
            _generator = new DbContextGenerator();
        }
        [SetUp]
        public void Setup()
        {
            _repo = new WeatherDataRepository(() => _generator.InitTestContext().FillData<WeatherData>().GetData(out _data).Context, _mapper);
        }

        [Test]
        public async Task BaseRepositoryTest_FindAllAsync_GetAllElements()
        {
            var ret = await _repo.FindAllAsync<WeatherDataDTO>();
            Assert.AreEqual(_data.Count(), ret.Count());
            Assert.AreEqual(_data.First().Station, ret.First().StationCode);
        }

        [Test]
        public async Task BaseRepositoryTest_FindAllAsync_WithCondition_GetElements()
        {
            var ret = await _repo.FindAllAsync<WeatherDataDTO>(v => v.Station == _data.First().Station);
            Assert.AreEqual(_data.First().Name, ret.First().StationName);
        }

        [Test]
        public async Task BaseRepositoryTest_GetSingleAsync_WithCondition_GetElements()
        {
            var ret = await _repo.GetSingleAsync<WeatherDataDTO>(v => v.Station == _data.First().Station);
            Assert.AreEqual(_data.First().Name, ret.StationName);
        }

        [Test]
        public async Task BaseRepositoryTest_GetFirstAsync_WithCondition_GetElements()
        {
            var ret = await _repo.GetFirstAsync<WeatherDataDTO>(v => v.Station == _data.First().Station);
            Assert.AreEqual(_data.First().Name, ret.StationName);
        }
    }
}