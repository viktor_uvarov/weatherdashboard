﻿namespace WeatherDashboard.BusinessLayer.Test
{
    using AutoFixture;
    using AutoMapper;
    using System.Collections.Generic;
    using WeatherDashboard.DAL.Test;
    using WeatherDashboard.Models.AutoMapper;

    public class BaseRepositoryTest<T, TRepo>
    {
        protected DbContextGenerator _generator;
        protected IMapper _mapper;
        protected TRepo _repo;
        protected IList<T> _data;
        protected Fixture _fixture;

        public BaseRepositoryTest()
        {
            _mapper = ModelMapper.GetMapper();
            _generator = new DbContextGenerator();
            _fixture = new Fixture();
        }
    }
}
