﻿namespace WeatherDashboard.Models.DbModels
{
    public interface IBaseEntity
    {
        int Id { get; set; }
    }
}
