﻿namespace WeatherDashboard.Models.DbModels
{
    using System;

    public partial class WeatherData : IBaseEntity
    {
        public int Id { get; set; }
        public string Province { get; set; }
        public string Station { get; set; }
        public DateTime Mdate { get; set; }
        public string Name { get; set; }
        public int Latitude { get; set; }
        public int Longtitude { get; set; }
        public int Tavg { get; set; }
        public int TMin { get; set; }
        public int TMax { get; set; }
        public int Prcp { get; set; }
        public int Snow { get; set; }
    }
}
