﻿using AutoMapper;
using System;
using WeatherDashboard.Models.Models.Results;

namespace WeatherDashboard.Models.AutoMapper
{
    public class ModelMapper
    {
        public static IConfigurationProvider GetConfig()
        {
            return new MapperConfiguration(cfg => {
                cfg.CreateMap<DbModels.WeatherData, WeatherDataDTO>()
                    .ForMember(d => d.StationName, s => s.MapFrom(m => m.Name))
                    .ForMember(d => d.StationCode, s => s.MapFrom(m => m.Station));
            });
        }
        public static IMapper GetMapper()
        {
            return new Mapper(GetConfig());
        }
    }
}
