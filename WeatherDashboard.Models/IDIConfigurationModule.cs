﻿namespace WeatherDashboard.Models
{
    using Microsoft.Extensions.DependencyInjection;
    public interface IDIConfigurationModule
    {
        void Configure(IServiceCollection services);
    }
}
