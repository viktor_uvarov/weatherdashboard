﻿namespace WeatherDashboard.Models.Models.Queries
{
    public class FindStationsQuery
    {
        public string Province { get; set; }
    }
}
