﻿namespace WeatherDashboard.Models.Models.Queries
{
    using System;
    public class GetStationDataQuery
    {
        public string Code { get; set; }
    }
}
