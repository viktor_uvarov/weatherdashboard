﻿namespace WeatherDashboard.Models.Models.Results
{
    using RiskFirst.Hateoas.Models;
    using System.Collections.Generic;

    public class WeatherStationListContainerDTO : LinkContainer
    {
        public IEnumerable<WeatherStationListDTO> Data { get; set; }
    }
    public class WeatherStationListDTO : LinkContainer
    {
        public string StationName { get; set; }
        public string StationCode { get; set; }
        public int YearStart { get; set; }
        public int YearFinish { get; set; }
    }
}
