﻿namespace WeatherDashboard.Models.Models.Results
{
    using System;
    using System.Collections.Generic;
    using RiskFirst.Hateoas.Models;

    public class WeatherStationDataDTO : LinkContainer
    {
        public string StationName { get; set; }
        public string StationCode { get; set; }
        public string StationProvinceCode { get; set; }
        public DateTime Start { get; set; }
        public DateTime Finish { get; set; }
        public double Latitude { get; set; }
        public double Longtinude { get; set; }
        public IEnumerable<WeatherStationDetailsDTO> Details { get; set; }

    }

    public class WeatherStationDetailsDTO
    {
        public DateTime Date { get; set; }
        public double Tavg { get; set; }
        public int Tmin { get; set; }
        public int Tmax { get; set; }
        public double Rain { get; set; }
        public double Snow { get; set; }
    }

}
