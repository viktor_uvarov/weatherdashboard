﻿namespace WeatherDashboard.Models.Models.Results
{
    public class WeatherDataDTO
    {
        public string StationName { get; set; }
        public string StationCode { get; set; }
    }
}
