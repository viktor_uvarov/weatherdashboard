﻿namespace WeatherDashboard.Models.Models.Results
{
    using RiskFirst.Hateoas.Models;
    using System.Collections.Generic;

    public class WeatherProviceContainerDTO : LinkContainer
    {
        public IEnumerable<WeatherProviceDTO> Data { get; set; }
    }

    public class WeatherProviceDTO
    {
        public string ProviceCode { get; set; }
    }
}
