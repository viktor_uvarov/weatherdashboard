﻿namespace WeatherDashboard.Test.Api
{
    using Moq;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using WeatherDashboard.Api;
    using WeatherDashboard.BusinessLayer;
    using WeatherDashboard.BusinessLayer.Services;
    using WeatherDashboard.Models.Models.Queries;
    using WeatherDashboard.Models.Models.Results;
    using WeatherDashboard.Utils;

    public class WeatherStationControllerTest
    {
        private Mock<IServiceContext> _serviceContext;
        private Mock<IHateoasHelper> _mockHateoasHelper;
        private Mock<IWeatherStationService> _mockWeatherDataService;
        private WeatherStationController _controller;

        public WeatherStationControllerTest()
        {
            _mockWeatherDataService = new Mock<IWeatherStationService>();
            _mockHateoasHelper = new Mock<IHateoasHelper>();
            _serviceContext = new Mock<IServiceContext>();
            _serviceContext.Setup(v => v.WeatherStationService).Returns(_mockWeatherDataService.Object);
            _controller = new WeatherStationController(_serviceContext.Object, _mockHateoasHelper.Object);
        }

        [SetUp]
        public void Setup()
        {
            _mockWeatherDataService.Reset();
            _mockHateoasHelper.Reset();
        }

        [Test]
        public async Task WeatherStationControllerTest_FindProvincesAsync_MethodsCalled()
        {
            var data = new WeatherProviceContainerDTO { Data = new List<WeatherProviceDTO>() };
            _mockWeatherDataService.Setup(v => v.FindProvincesAsync()).ReturnsAsync(data.Data);
            var ret = await _controller.FindProvincesAsync();
            _mockWeatherDataService.Verify(v => v.FindProvincesAsync(), Times.Once);
            _mockHateoasHelper.Verify(v => v.AddLink(It.Is<WeatherProviceContainerDTO>(vv => vv.Data == data.Data)), Times.Once);
            Assert.AreEqual(data.Data, ret.Data);
        }

        [Test]
        public async Task WeatherStationControllerTest_FindStationsAsync_MethodsCalled()
        {
            var data = new WeatherStationListContainerDTO { Data = new List<WeatherStationListDTO>() };
            var query = new FindStationsQuery();
            _mockWeatherDataService.Setup(v => v.FindStationsAsync(It.IsAny<FindStationsQuery>())).ReturnsAsync(data.Data);
            var ret = await _controller.FindStationsAsync(query);
            _mockWeatherDataService.Verify(v => v.FindStationsAsync(It.Is<FindStationsQuery>(vv => vv == query)), Times.Once);
            _mockHateoasHelper.Verify(v => v.AddLink(It.Is<WeatherStationListContainerDTO>(vv => vv.Data == data.Data)), Times.Once);
            Assert.AreEqual(data.Data, ret.Data);
        }

        [Test]
        public async Task WeatherStationControllerTest_GetStationDataAsync_MethodsCalled()
        {
            var data = new WeatherStationDataDTO();
            var query = new GetStationDataQuery();
            _mockWeatherDataService.Setup(v => v.GetStationDataAsync(It.IsAny<GetStationDataQuery>())).ReturnsAsync(data);
            var ret = await _controller.GetStationDataAsync(query);
            _mockWeatherDataService.Verify(v => v.GetStationDataAsync(It.Is<GetStationDataQuery>(vv => vv == query)), Times.Once);
            _mockHateoasHelper.Verify(v => v.AddLink(It.Is<WeatherStationDataDTO>(vv => vv == data)), Times.Once);
            Assert.AreEqual(data, ret);
        }
    }
}
