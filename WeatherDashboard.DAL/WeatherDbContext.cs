﻿namespace WeatherDashboard.DAL
{
    using Microsoft.EntityFrameworkCore;
    using WeatherDashboard.Models.DbModels;

    public partial class WeatherDbContext : DbContext, IWeatherDbContext
    {
        public WeatherDbContext()
        {
        }

        public WeatherDbContext(DbContextOptions<WeatherDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<WeatherData> WeatherData { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql(Models.AppConfiguration.MySQLConnectionString);
            }
        }

        public void DetectChanges()
        {
            this.ChangeTracker.DetectChanges();
        }
    }
}
