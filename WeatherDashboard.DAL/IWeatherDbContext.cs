﻿namespace WeatherDashboard.DAL
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.ChangeTracking;
    using Microsoft.EntityFrameworkCore.Infrastructure;
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using WeatherDashboard.Models.DbModels;

    public interface IWeatherDbContext : IDisposable
    {
        DatabaseFacade Database { get; }
        ChangeTracker ChangeTracker { get; }
        DbSet<T> Set<T>() where T : class;
        int SaveChanges();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

        void DetectChanges();

        DbSet<WeatherData> WeatherData { get; set; }

    }
}
