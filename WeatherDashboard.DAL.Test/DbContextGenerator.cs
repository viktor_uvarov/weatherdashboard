﻿namespace WeatherDashboard.DAL.Test
{
    using AutoFixture;
    using Microsoft.EntityFrameworkCore;
    using Moq;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using WeatherDashboard.Models.DbModels;

    public class DbContextGenerator
    {
        private Fixture _fixture = new Fixture();
        private Dictionary<Type, dynamic> _data;
        
        public IWeatherDbContext Context { get; private set;}
        
        #region Test DB
        public DbContextGenerator InitTestContext()
        {
            _data = new Dictionary<Type, dynamic>();
            var options = new DbContextOptionsBuilder<WeatherDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            Context = new WeatherDbContext(options);
            return this;
        }
        public DbContextGenerator GetData<T>(out IList<T> data)
        {
            data = _data[typeof(T)] as IList<T>;
            return this;
        }
        public DbContextGenerator FillData<T>(IList<T> data = null) where T : class
        {
            var p = Context.GetType().GetProperties().Where(t => t.PropertyType == typeof(DbSet<T>)).FirstOrDefault();
            if (p == null) throw new ArgumentException($"The the DbSet of the type {typeof(T)} is absent in this context");
            _data[typeof(T)] = data ?? _fixture.CreateMany<T>(10).ToList();
            ((DbSet<T>)p.GetValue(Context)).AddRange(_data[typeof(T)]);
            Context.SaveChanges();
            return this;
        }
        #endregion

        #region Mock context
        public Mock<IWeatherDbContext> GetMockContext()
        {
            var context = new Mock<IWeatherDbContext>();
            return context;
        }
        #endregion
    }
}
