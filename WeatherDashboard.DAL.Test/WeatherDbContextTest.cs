namespace WeatherDashboard.DAL.Test
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.ChangeTracking;
    using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
    using Microsoft.EntityFrameworkCore.Metadata;
    using Microsoft.EntityFrameworkCore.Metadata.Internal;
    using Moq;
    using NUnit.Framework;
    using AutoFixture;

    public class WeatherDbContextTest
    {
        //private IWeatherDbContext _context;
        private DbContextGenerator _generator;
        private Mock<DbContextOptionsBuilder<WeatherDbContext>> _builder;
        private Fixture _fixture;

        public WeatherDbContextTest()
        {
            _generator = new DbContextGenerator();
            _fixture = new Fixture();
        }

        [SetUp]
        public void Setup()
        {
            _builder = new Mock<DbContextOptionsBuilder<WeatherDbContext>>();
            _builder.Setup(v => v.Options).Returns(new Mock<DbContextOptions<WeatherDbContext>>().Object);
            //_context = _generator.GetTestContext(_builder.Object);
        }

        [Test]
        public void WeatherDbContextTest_Initialized()
        {
            //_builder.Verify(v => v.UseMySql(It.IsAny<string>(), null), Times.Once);
            Assert.Pass();
        }
    }
}