﻿namespace WeatherDashboard.BusinessLayer
{
    public interface IServiceContext
    {
        Services.IWeatherStationService WeatherStationService { get; }
    }

    public class ServiceContext : IServiceContext
    {
        private readonly System.Lazy<Services.IWeatherStationService> _lazyWeatherStationService;
        public ServiceContext(IRepositoryContext context, AutoMapper.IMapper mapper)
        {
            _lazyWeatherStationService = new System.Lazy<Services.IWeatherStationService>(() => new Services.WeatherStationService(context, mapper));
        }

        public Services.IWeatherStationService WeatherStationService => _lazyWeatherStationService.Value;
    }
}
