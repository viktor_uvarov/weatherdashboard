﻿namespace WeatherDashboard.BusinessLayer.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AutoMapper;
    using WeatherDashboard.Models.Models.Queries;
    using WeatherDashboard.Models.Models.Results;

    public interface IWeatherStationService
    {
        Task<IEnumerable<WeatherStationListDTO>> FindStationsAsync(FindStationsQuery query);
        Task<IEnumerable<WeatherProviceDTO>> FindProvincesAsync();
        Task<WeatherStationDataDTO> GetStationDataAsync(GetStationDataQuery query);
    }

    public class WeatherStationService : BaseService, IWeatherStationService
    {
        public WeatherStationService(IRepositoryContext repositoriesContext, IMapper mapper) : base(repositoriesContext, mapper)
        {
        }

        public async Task<IEnumerable<WeatherStationListDTO>> FindStationsAsync(FindStationsQuery query)
        {
            return await RepositoriesContext.WeatherDataRepository.FindStationsAsync(query);
        }
        public async Task<IEnumerable<WeatherProviceDTO>> FindProvincesAsync()
        {
            return await RepositoriesContext.WeatherDataRepository.FindProvincesAsync();
        }

        public async Task<WeatherStationDataDTO> GetStationDataAsync(GetStationDataQuery query)
        {
            var ret = await RepositoriesContext.WeatherDataRepository.GetStationDataAsync(query);
            ret.Details = await RepositoriesContext.WeatherDataRepository.GetStationDetailsAsync(query);
            return ret;
        }
    }
}
