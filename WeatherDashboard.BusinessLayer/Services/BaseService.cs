﻿namespace WeatherDashboard.BusinessLayer.Services
{
    using AutoMapper;

    public class BaseService
    {
        protected readonly IRepositoryContext RepositoriesContext;
        protected readonly IMapper Mapper;
        protected BaseService(IRepositoryContext repositoriesContext, IMapper mapper)
        {
            RepositoriesContext = repositoriesContext;
            Mapper = mapper;
        }
    }
}
