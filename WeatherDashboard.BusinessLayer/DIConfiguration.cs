﻿namespace WeatherDashboard.BusinessLayer
{
    using AutoMapper;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using System;
    using WeatherDashboard.DAL;
    using WeatherDashboard.Models;

    public class DIConfiguration : IDIConfigurationModule
    {
        public void Configure(IServiceCollection services)
        {
            var mapConfig = Models.AutoMapper.ModelMapper.GetConfig();
            services.AddSingleton<IMapper>(mapConfig.CreateMapper());
            services.AddSingleton<IConfigurationProvider>(mapConfig);

            services.AddTransient<IWeatherDbContext, WeatherDbContext>();
            services.AddTransient<Func<IWeatherDbContext>, Func<WeatherDbContext>>(service =>
            {
                return () => new WeatherDbContext();
            });

            services.AddTransient<IRepositoryContext, RepositoryContext>();
            services.AddTransient<IServiceContext, ServiceContext>();

            services.AddDbContextPool<WeatherDbContext>(
                options => options.UseMySql("Server=localhost;Database=ef;User=root;Password=123456;",
                    mySqlOptions =>
                    {
                        mySqlOptions.ServerVersion(new Version(8, 0, 18), Pomelo.EntityFrameworkCore.MySql.Infrastructure.ServerType.MySql);
                    }
            ));
        }
    }
}
