﻿namespace WeatherDashboard.BusinessLayer.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using WeatherDashboard.Models.DbModels;

    public interface IBaseRepository<T> where T : class, IBaseEntity, new()
    {
        Task<IEnumerable<TResult>> FindAllAsync<TResult>();
        Task<IEnumerable<TResult>> FindAllAsync<TResult>(Expression<Func<T, bool>> predicate);
        Task<TResult> GetSingleAsync<TResult>(Expression<Func<T, bool>> predicate);
        Task<TResult> GetFirstAsync<TResult>(Expression<Func<T, bool>> predicate);
    }
}
