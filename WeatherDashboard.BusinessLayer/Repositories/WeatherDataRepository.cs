﻿namespace WeatherDashboard.BusinessLayer.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using Microsoft.EntityFrameworkCore;
    using WeatherDashboard.DAL;
    using WeatherDashboard.Models.DbModels;
    using WeatherDashboard.Models.Models.Queries;
    using WeatherDashboard.Models.Models.Results;

    public interface IWeatherDataRepository : IBaseRepository<WeatherData>
    {
        Task<IEnumerable<WeatherStationListDTO>> FindStationsAsync(FindStationsQuery query);
        Task<IEnumerable<WeatherProviceDTO>> FindProvincesAsync();
        Task<WeatherStationDataDTO> GetStationDataAsync(GetStationDataQuery query);
        Task<IEnumerable<WeatherStationDetailsDTO>> GetStationDetailsAsync(GetStationDataQuery query);

    }

    public class WeatherDataRepository : BaseRepository<WeatherData>, IWeatherDataRepository
    {
        public WeatherDataRepository(Func<IWeatherDbContext> contextFactory, IMapper mapper) : base(contextFactory, mapper)
        {
        }

        public async Task<IEnumerable<WeatherStationListDTO>> FindStationsAsync(FindStationsQuery query)
        {
            using (var context = ContextFactory())
            {
                return await context.WeatherData
                    .Where(v => v.Province == query.Province)
                    .GroupBy(v => new { v.Name, v.Station })
                    .Select(v =>
                     new WeatherStationListDTO
                     {
                         StationName = v.Key.Name,
                         StationCode = v.Key.Station,
                         YearStart = v.Min(vv => vv.Mdate.Year),
                         YearFinish = v.Max(vv => vv.Mdate.Year)
                     }).OrderBy(v => v.StationName)
                       .ToListAsync();
            }
        }

        public async Task<IEnumerable<WeatherProviceDTO>> FindProvincesAsync()
        {
            using (var context = ContextFactory())
            {
                return await context.WeatherData.Select(v => v.Province).Distinct().Select(v => new WeatherProviceDTO { ProviceCode = v })
                    .OrderBy(v => v.ProviceCode).ToListAsync();
            }
        }

        public async Task<WeatherStationDataDTO> GetStationDataAsync(GetStationDataQuery query)
        {
            using (var context = ContextFactory())
            {
                return await context.WeatherData
                    .Where(v => v.Station == query.Code)
                    .GroupBy(v => new { v.Name, v.Longtitude, v.Latitude, v.Station, v.Province })
                    .Select(v =>
                            new WeatherStationDataDTO
                            {
                                StationName = v.Key.Name,
                                Longtinude = v.Key.Longtitude,
                                Latitude = v.Key.Latitude,
                                StationCode = v.Key.Station,
                                StationProvinceCode = v.Key.Province,
                                Start = v.Min(vv => vv.Mdate),
                                Finish = v.Max(vv => vv.Mdate)
                            })
                    .FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<WeatherStationDetailsDTO>> GetStationDetailsAsync(GetStationDataQuery query)
        {
            using (var context = ContextFactory())
            {
                return await context.WeatherData
                    .Where(v => v.Station == query.Code)
                    .OrderByDescending(v => v.Mdate)
                    .Take(12)
                    .Select(v =>
                            new WeatherStationDetailsDTO
                            {
                                Date = v.Mdate,
                                Tavg = v.Tavg,
                                Tmin = v.TMin,
                                Tmax = v.TMax,
                                Rain = (v.Prcp - v.Snow / 10.0) < 0 ? 0 : v.Prcp - v.Snow / 10.0,
                                Snow = v.Snow / 10.0
                            })
                    .OrderBy(v => v.Date)
                    .ToListAsync();
            }
        }
    }
}
