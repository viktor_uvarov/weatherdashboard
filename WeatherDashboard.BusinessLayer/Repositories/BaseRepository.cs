﻿namespace WeatherDashboard.BusinessLayer.Repositories
{
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using WeatherDashboard.DAL;
    using WeatherDashboard.Models.DbModels;

    public abstract class BaseRepository<T> : IBaseRepository<T> where T : class, IBaseEntity, new()
    {
        protected readonly IMapper Mapper;
        protected readonly Func<IWeatherDbContext> ContextFactory;

        protected BaseRepository(Func<IWeatherDbContext> contextFactory, IMapper mapper)
        {
            Mapper = mapper;
            ContextFactory = contextFactory;
        }
        public async Task<IEnumerable<TResult>> FindAllAsync<TResult>()
        {
            using (var context = ContextFactory())
            {
                return await context.Set<T>().ProjectTo<TResult>(Mapper.ConfigurationProvider).ToListAsync();
            }
        }
        public async Task<IEnumerable<TResult>> FindAllAsync<TResult>(Expression<Func<T, bool>> predicate)
        {
            using (var context = ContextFactory())
            {
                return await context.Set<T>().Where(predicate).ProjectTo<TResult>(Mapper.ConfigurationProvider).ToListAsync();
            }
        }

        public async Task<TResult> GetSingleAsync<TResult>(Expression<Func<T, bool>> predicate)
        {
            using (var context = ContextFactory())
            {
                return await context.Set<T>().Where(predicate).ProjectTo<TResult>(Mapper.ConfigurationProvider).SingleOrDefaultAsync();
            }
        }
        public async Task<TResult> GetFirstAsync<TResult>(Expression<Func<T, bool>> predicate)
        {
            using (var context = ContextFactory())
            {
                return await context.Set<T>().Where(predicate).ProjectTo<TResult>(Mapper.ConfigurationProvider).FirstOrDefaultAsync();
            }
        }
    }
}
