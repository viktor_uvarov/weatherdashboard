﻿namespace WeatherDashboard.BusinessLayer
{
    using WeatherDashboard.DAL;

    public interface IRepositoryContext
    {
        Repositories.IWeatherDataRepository WeatherDataRepository { get; }
    }

    public class RepositoryContext : IRepositoryContext
    {
        private readonly System.Lazy<Repositories.IWeatherDataRepository> _lazyWeatherDataRepository;
        public RepositoryContext(System.Func<IWeatherDbContext> context, AutoMapper.IMapper mapper)
        {
            _lazyWeatherDataRepository = new System.Lazy<Repositories.IWeatherDataRepository>(() => new Repositories.WeatherDataRepository(context, mapper));
        }

        public Repositories.IWeatherDataRepository WeatherDataRepository => _lazyWeatherDataRepository.Value;
    }
}
