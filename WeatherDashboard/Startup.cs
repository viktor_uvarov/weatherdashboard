using System;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WeatherDashboard.Api;
using WeatherDashboard.BusinessLayer;
using WeatherDashboard.Utils;

namespace WeatherDashboard
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            Models.AppConfiguration.MySQLConnectionString = Configuration["MySQLConnectionString"];
            services.AddRazorPages();
            services.AddServerSideBlazor().AddCircuitOptions(options => { options.DetailedErrors = true; });
            services.AddCors(options =>
            {
                options.AddPolicy("CORS",
                corsPolicyBuilder => corsPolicyBuilder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });

            services.AddSingleton<WeatherStationController>();
            services.AddSingleton<IHateoasHelper>(new HateoasHelper());

            InitDependencyInjection(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCors("CORS");

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }

        private void InitDependencyInjection(IServiceCollection services)
        {
            InitAssemblyDependencyInjection(typeof(Startup).Assembly, services);
            InitAssemblyDependencyInjection(typeof(ServiceContext).Assembly, services);
        }
        private void InitAssemblyDependencyInjection(Assembly assembly, IServiceCollection services)
        {
            foreach (Type type in assembly.GetTypes().Where(t => t.GetInterfaces().Contains(typeof(Models.IDIConfigurationModule))))
            {
                var instance = (Models.IDIConfigurationModule)Activator.CreateInstance(type);
                instance?.Configure(services);
            }
        }
    }
}
