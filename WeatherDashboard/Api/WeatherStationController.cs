﻿namespace WeatherDashboard.Api
{
    using Microsoft.AspNetCore.Mvc;
    using RiskFirst.Hateoas;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using WeatherDashboard.BusinessLayer;
    using WeatherDashboard.Models.Models.Queries;
    using WeatherDashboard.Models.Models.Results;
    using WeatherDashboard.Utils;

    [Route("api/[controller]")]
    public class WeatherStationController : BaseController
    {
        public WeatherStationController(IServiceContext services, IHateoasHelper hhelper) : base(services, hhelper)
        {
        }

        [HttpPost]
        [Route("findstations")]
        public async Task<WeatherStationListContainerDTO> FindStationsAsync([FromBody] FindStationsQuery query)
        {
            var model = new WeatherStationListContainerDTO { Data = await ServiceContext.WeatherStationService.FindStationsAsync(query) };
            HateoasHelper.AddLink(model);
            return model;
        }
        [HttpGet]
        [Route("findprovinces")]
        public async Task<WeatherProviceContainerDTO> FindProvincesAsync()
        {
            var model = new WeatherProviceContainerDTO { Data = await ServiceContext.WeatherStationService.FindProvincesAsync() };
            HateoasHelper.AddLink(model);
            return model;
        
        }
        [HttpPost]
        [Route("getstationdata")]
        public async Task<WeatherStationDataDTO> GetStationDataAsync([FromBody] GetStationDataQuery query)
        {
            var model = await ServiceContext.WeatherStationService.GetStationDataAsync(query);
            HateoasHelper.AddLink(model);
            return model;
        }
    }
}
