﻿namespace WeatherDashboard.Api
{
    using Microsoft.AspNetCore.Mvc;
    using RiskFirst.Hateoas;
    using System.Net.Mime;
    using WeatherDashboard.BusinessLayer;
    using WeatherDashboard.Utils;

    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    public class BaseController : ControllerBase
    {
        protected readonly IServiceContext ServiceContext;
        protected readonly IHateoasHelper HateoasHelper;

        /// <summary>
        /// Protected constructor that is used by child classes
        /// </summary>
        /// <param name="services">Service context</param>
        protected BaseController(IServiceContext services, IHateoasHelper hhelper)
        {
            ServiceContext = services;
            HateoasHelper = hhelper;
        }
    }
}
