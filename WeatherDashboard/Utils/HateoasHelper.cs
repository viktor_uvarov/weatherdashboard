﻿using RiskFirst.Hateoas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using WeatherDashboard.Models.Models.Results;

namespace WeatherDashboard.Utils
{
    public interface IHateoasHelper
    {
        T AddLink<T>(T item);
        IEnumerable<T> AddLinks<T>(IEnumerable<T> items);
    }
    public class HateoasHelper : IHateoasHelper
    {
        private static List<LinkDescription> _modelTypes;
        
        public HateoasHelper()
        {
            InitModels();
        }
        public void InitModels()
        {
            if (_modelTypes != null) return;
            _modelTypes = new List<LinkDescription>();
            _modelTypes.Add(new LinkDescription().Fill<WeatherProviceContainerDTO>("self", "GET", "WeatherStationController/FindProvincesAsync", "/api/weatherstation/findprovinces", null));
            _modelTypes.Add(new LinkDescription().Fill<WeatherProviceContainerDTO>("findstations", "POST", "WeatherStationController/FindStationsAsync", "/api/weatherstation/findstations/{0}", v => "<Province code>"));
            _modelTypes.Add(new LinkDescription().Fill<WeatherStationListContainerDTO>("self", "POST", "WeatherStationController/FindStationsAsync", "/api/weatherstation/findstations/{0}", v => "<Station province>"));
            _modelTypes.Add(new LinkDescription().Fill<WeatherStationListContainerDTO>("findprovinces", "GET", "WeatherStationController/FindProvincesAsync", "/api/weatherstation/findprovinces", null));
            _modelTypes.Add(new LinkDescription().Fill<WeatherStationListContainerDTO>("getstationdata", "POST", "WeatherStationController/GetStationDataAsync", "/api/weatherstation/getstationdata/{0}", v => "<Station code>"));
            _modelTypes.Add(new LinkDescription().Fill<WeatherStationDataDTO>("self", "POST", "WeatherStationController/GetStationDataAsync", "/api/weatherstation/getstationdata/{0}", v => v.StationCode));
            _modelTypes.Add(new LinkDescription().Fill<WeatherStationDataDTO>("findstations", "POST", "WeatherStationController/FindStationsAsync", "/api/weatherstation/findstations/{0}", v => v.StationProvinceCode));
        }

        public T AddLink<T>(T item)
        {
            foreach (var link in _modelTypes.Where(v => v.Type == item.GetType()))
            {
                var l = new Link 
                { 
                    Method = link.Method, 
                    Href = link.Parameter == null ? link.Href : String.Format(link.Href, link.Parameter(item)),
                    Rel = link.Rel
                };
                (item as LinkContainer).Links.Add(link.Key, l);
            }
            return item;
        }

        public IEnumerable<T> AddLinks<T>(IEnumerable<T> items)
        {
            foreach (var i in items)
            {
                AddLink(i);
            }
            return items;
        }
    }

    internal class LinkDescription
    {
        public Type Type { get; private set; }
        public string Key { get; private set; }
        public string Method { get; private set; }
        public string Href { get; private set; }
        public string Rel { get; private set; }
        public dynamic Parameter { get; private set; }

        public LinkDescription Fill<T>(string key, string method, string rel, string href, Func<T, string> par)
        {
            Type = typeof(T);
            Key = key;
            Method = method;
            Href = href;
            Rel = rel;
            Parameter = par;
            return this;
        }
    }
}
